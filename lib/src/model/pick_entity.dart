import 'dart:io';

import 'package:photo_manager/photo_manager.dart';
import 'package:wechat_camera_picker/wechat_camera_picker.dart';
//拍照返回的对象
class PickEntity{

  const PickEntity({required this.entityFile,this.assetEntity});

  final AssetEntity? assetEntity;
  final File entityFile;
}
