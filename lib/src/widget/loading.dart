import 'package:flutter/material.dart';


class LoadingTool {
  BuildContext? loadingContext;
  void showLoading(BuildContext context,String text){
    showDialog<Loading>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          loadingContext = context;
          return Loading(
             text: text,
          );
        }
    );
  }
  void dismiss(){
    if(loadingContext != null)
    Navigator.pop(loadingContext!);
  }
}

class Loading extends StatefulWidget {
  const Loading({Key? key,required this.text}) : super(key: key);

  final String text;
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Center(
        child: SizedBox(
          width: 120.0,
          height: 120.0,
          child: Container(
            decoration: const ShapeDecoration(
              color: Color(0xffffffff),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(8.0),
                ),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                const CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Color(0xffAA1F52))),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 20.0,
                  ),
                  child: Text(widget.text),
                ),
              ],
            ),
          ),
        ),
      ),
    );

  }
}
